(function (global) {
  function ngBootstrapPackIndex(pkgName) {
    ;
  }

  var ngBootstrapPackageNames = [
    'accordion',
    'alert',
    'bundles',
    'buttons',
    'carousel',
    'collapse',
    'dropdown',
    'esm',
    'modal',
    'pagination',
    'popover',
    'progressbar',
    'rating',
    'tabset',
    'timepicker',
    'tooltip',
    'typeahead',
    'util'
  ];

  var config = {
    paths: {
      'npm:': 'node_modules/'
    },
    map: {
      app: 'dist',

      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
      '@angular/upgrade': 'npm:@angular/upgrade/bundles/upgrade.umd.js',
      
      'rxjs':                      'npm:rxjs',
      'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
      //'@ng-bootstrap/ng-bootstrap': 'node_modules/@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js',
      '@ng-bootstrap': 'node_modules/@ng-bootstrap',
      '@ng-bootstrap/ng-bootstrap': 'node_modules/@ng-bootstrap/ng-bootstrap'
    },
    packages: {
      app: {
        main: './main.js',
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      },
      
      '@ng-bootstrap/ng-bootstrap': {main: '/bundles/ng-bootstrap.js', defaultExtension: 'js'},
    }
  };

  ngBootstrapPackageNames.forEach(function(pkgName) {
    config.packages['@ng-bootstrap/ng-bootstrap/' + pkgName] = {main: 'index.js', defaultExtension: 'js'}
  });

  System.config(config);
})(this);
