import { Component, Input } from '@angular/core';
import { Transaction } from './transaction.class';
import { Company } from './company.class'

@Component({
    selector: 'transaction-list-company',
    template: `
        <span class="text-primary text-bold">{{company.name}}</span><br/>
        <span class="text-muted">{{company.domain}}</span><br/>
        <span>{{company.country}}</span>
    `
})
export class TransactionListCompanyComponent {
    @Input()
    company: Company;
}

@Component({
    selector: 'transaction-list',
    template: `
        <table class="table table-striped">
            <tr>
                <th>UUID</th>
                <th>Timestamp</th>
                <th>Sender</th>
                <th>Reciever</th>
                <th>Amount, USD</th>
                <th>Suspiciousness</th>
            </tr>
            <ng-container *ngFor="let tran of transactions">
            <tr>
                <th scope="row">{{tran.uuid}}</th>
                <td>{{tran.timestamp}}</td>
                <td><transaction-list-company [company]="getCompany(tran.sender)"></transaction-list-company></td>
                <td><transaction-list-company [company]="getCompany(tran.reciever)"></transaction-list-company></td>
                <td>{{tran.amount}}</td>
                <td>{{tran.suspiciousness}}</td>
            </tr>
            </ng-container>
        </table>
    `
})
export class TransactionListComponent {
    @Input()
    transactions: Transaction[];

    @Input()
    companies: Company[];

    selectedTransaction: Transaction;
    selectedSender: Company;
    selectedReceiver: Company;

    getCompany(name: string) {
        for (let company of this.companies) {
            if (company.name == name)
                return company
        }
    }
}