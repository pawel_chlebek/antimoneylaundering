import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Transaction } from './transaction.class';
import { Company } from './company.class'


@Injectable()
export class DataService {
    private static urls: string[] = [
        "https://f146d0f22c0546489107a179e4783c5d-vp0.us.blockchain.ibm.com:5001/chaincode",
        "https://f146d0f22c0546489107a179e4783c5d-vp1.us.blockchain.ibm.com:5001/chaincode",
        "https://f146d0f22c0546489107a179e4783c5d-vp2.us.blockchain.ibm.com:5001/chaincode",
        "https://f146d0f22c0546489107a179e4783c5d-vp3.us.blockchain.ibm.com:5001/chaincode",
    ];

    constructor(private http: Http) {}

    private getUrl(): string {
        return DataService.urls[0];
    }

    private extractCompaniesData(res: Response) {

    }

    private catchError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
          const body = error.json() || '';
          const err = body.error || JSON.stringify(body);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    getCompanies()/*: Observable<Company[]>*/ {
        let request = `{
          "jsonrpc": "2.0",
          "method": "query",
          "params": {
            "type": 1,
            "chaincodeID": {
              "name": "60d1bc705a31fcdf8203bd63f3214e33a26abcbbb6e3d2c01a9c511d1e16fa15afd5cce3120fa6b85d6d9c2252ba1d4bd29c8bc4044852916196f24681ecf6e9"
            },
            "ctorMsg": {
              "function": "listCompany",
              "args": [

              ]
            },
            "secureContext": "user_type1_0"
          },
          "id": 1234
        }`

        let retval = this.http.post(this.getUrl(), request)
                              .flatMap((company) => JSON.parse(company.json()['result']['message'])['columns']);
                        /*.map((person) => )
                        .catch(this.catchError)*/
        return retval
    }
}