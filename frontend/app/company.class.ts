export class Company {
    name: string;
    country: string;
    domain: string;

    constructor(name: string, country: string, domain: string) {
        this.name = name;
        this.country = country;
        this.domain = domain;
    }

    static DOMAINS = [
        "Finance",
        "Unkonow domain"
    ];
}