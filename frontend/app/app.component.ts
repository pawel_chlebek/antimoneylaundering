import { Component, OnInit } from '@angular/core'
import { Company } from './company.class'
import { Transaction } from './transaction.class'
import { DataService } from './data.service'


@Component({
  selector: 'my-app',
  template: `
      <transaction-list [transactions]="transactions" [companies]="companies"></transaction-list>
  `
})
export class AppComponent implements OnInit {
    companies: Company[];
    transactions: Transaction[];

    constructor(private dataService: DataService) {}

    ngOnInit() {
        this.dataService.getCompanies().submit()

        /*this.companies = [
            new Company("Finance1", "USA", "Finance"),
            new Company("Finance2", "POL", "Finance"),
            new Company("Bosch", "DEU", "Automotive"),
            new Company("Valeo", "FRA", "Automotive")
        ]

        this.transactions = [
            new Transaction("0", "Finance1", "Finance2", 500, "2016-11-17 06:00:00", 0.1),
            new Transaction("1", "Finance1", "Finance2", 1500, "2016-11-17 06:00:00", 0.1),
            new Transaction("2", "Finance1", "Bosch", 5000000, "2016-11-17 06:00:00", 0.7),
            new Transaction("3", "Bosch", "Valeo", 12500, "2016-11-17 06:00:00", 0.1)
        ]*/
    }
}
