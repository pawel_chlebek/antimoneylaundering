export class Transaction {
    uuid: string;
    sender: string;
    reciever: string;
    amount: number;
    timestamp: string;
    suspiciousness: number;

    constructor(uuid: string, sender: string, reciever: string, amount: number, timestamp: string, suspiciousness: number) {
        this.uuid = uuid;
        this.sender = sender;
        this.reciever = reciever;
        this.amount = amount;
        this.timestamp = timestamp;
        this.suspiciousness = suspiciousness;
    }
}
