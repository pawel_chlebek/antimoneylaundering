package main

import (
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/core/util"
	"encoding/json"
	"strconv"
)

type BankChaincode struct {
}

// Init callback representing the invocation of a chaincode
// This chaincode will manage two accounts A and B and will transfer X units from A to B upon invoke
func (t *BankChaincode) Init(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	//var err error

	saveOK, err := t.saveBankOwner(stub, args)
	if !saveOK && err != nil {
		return nil, errors.New("Error saveCompanyOwner")
	}

	saveOK, err = t.savePublicInformation(stub, args)
	if !saveOK && err != nil {
		return nil, errors.New("Error savePublicInformation")
	}

	saveOK, err = t.initCompanyTable(stub, args)
	if !saveOK && err != nil {
		return nil, errors.New("Error initCompanyTable")
	}

	saveOK, err = t.initTransactionTable(stub, args)
	if !saveOK && err != nil {
		return nil, errors.New("Error initCompanyTable")
	}

	return nil, nil
}

func (t *BankChaincode) saveBankOwner(stub shim.ChaincodeStubInterface, args []string) (bool, error) {
	//ownerCertificate, err := stub.GetCallerMetadata()
	//if err != nil {
	//	return false, errors.New("Failed getting metadata.")
	//}
	//if len(ownerCertificate) == 0 {
	//	return false, errors.New("Invalid admin certificate. Empty.")
	//}
	//stub.PutState("ownerCert", ownerCertificate)
	err := stub.PutState("ownerName", []byte(args[0]))
	if err != nil {
		return false, err
	}

	return true, nil
}

func (t *BankChaincode) savePublicInformation(stub shim.ChaincodeStubInterface, args []string) (bool, error) {
	var err error
	var size int
	size = len(args)

	for i := 1; i < size; i = i + 2 {
		name := args[i]
		json := args[i + 1]

		err = stub.PutState(name, []byte(json))
		if err != nil {
			return false, err
		}
	}

	return true, nil
}

func (t *BankChaincode) initCompanyTable(stub shim.ChaincodeStubInterface, args []string) (bool, error) {
	err := stub.CreateTable("Company", []*shim.ColumnDefinition{
		&shim.ColumnDefinition{Name: "CompanyName", Type: shim.ColumnDefinition_STRING, Key: true},
		&shim.ColumnDefinition{Name: "ChaincodeID", Type: shim.ColumnDefinition_STRING, Key: false},
		&shim.ColumnDefinition{Name: "Country", Type: shim.ColumnDefinition_STRING, Key: false},
		&shim.ColumnDefinition{Name: "Domain", Type: shim.ColumnDefinition_STRING, Key: false},
	})
	if err != nil {
		return false, err
	}

	/*ok, errInsert := stub.InsertRow("Company", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyA"}},
			&shim.Column{Value: &shim.Column_String_{String_: "01a37fc6a7e4006184b1e9f06747207ce566033291171d8772fadd72e5374b98b6b3d5095565f3b83ee6a94cbecd8f8fbc5c6ebff50793f39f9b6b4a098ccf93"}},
			&shim.Column{Value: &shim.Column_String_{String_: "UK"}},
			&shim.Column{Value: &shim.Column_String_{String_: "Finance"}},
		}, })

	if !ok && errInsert != nil {
		return false, errors.New("Error InsertRow Company")
	}

	ok, errInsert = stub.InsertRow("Company", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyB"}},
			&shim.Column{Value: &shim.Column_String_{String_: "8b82da247680fa23ee590fe6c96bb91c6eb4a88e4b8ebc7603aa6e55235193d92205b1db49795966230d4a416e005b69cd5fb7382089d341804d088a73bbcf8d"}},
			&shim.Column{Value: &shim.Column_String_{String_: "UK"}},
			&shim.Column{Value: &shim.Column_String_{String_: "Finance"}},
		}, })

	if !ok && errInsert != nil {
		return false, errors.New("Error InsertRow Company")
	}

	ok, errInsert = stub.InsertRow("Company", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyC"}},
			&shim.Column{Value: &shim.Column_String_{String_: "8b82da247680fa23ee590fe6c96bb91c6eb4a88e4b8ebc7603aa6e55235193d92205b1db49795966230d4a416e005b69cd5fb7382089d341804d088a73bbcf8d"}},
			&shim.Column{Value: &shim.Column_String_{String_: "PL"}},
			&shim.Column{Value: &shim.Column_String_{String_: "Finance"}},
		}, })

	if !ok && errInsert != nil {
		return false, errors.New("Error InsertRow Company")
	}

	ok, errInsert = stub.InsertRow("Company", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyX"}},
			&shim.Column{Value: &shim.Column_String_{String_: "8b82da247680fa23ee590fe6c96bb91c6eb4a88e4b8ebc7603aa6e55235193d92205b1db49795966230d4a416e005b69cd5fb7382089d341804d088a73bbcf8d"}},
			&shim.Column{Value: &shim.Column_String_{String_: "PL"}},
			&shim.Column{Value: &shim.Column_String_{String_: "Car"}},
		}, })

	if !ok && errInsert != nil {
		return false, errors.New("Error InsertRow Company")
	}*/

	return true, nil
}

//Init table for notifications
func (t *BankChaincode) initTransactionTable(stub shim.ChaincodeStubInterface, args []string) (bool, error) {
	err := stub.CreateTable("Transaction", []*shim.ColumnDefinition{
		&shim.ColumnDefinition{Name: "ID", Type: shim.ColumnDefinition_STRING, Key: true},
		&shim.ColumnDefinition{Name: "Sender", Type: shim.ColumnDefinition_STRING, Key: false},
		&shim.ColumnDefinition{Name: "Recipient", Type: shim.ColumnDefinition_STRING, Key: false},
		&shim.ColumnDefinition{Name: "Amount", Type: shim.ColumnDefinition_INT32, Key: false},
		&shim.ColumnDefinition{Name: "Timestamp", Type: shim.ColumnDefinition_INT64, Key: false},
		&shim.ColumnDefinition{Name: "Suspicion", Type: shim.ColumnDefinition_STRING, Key: false},
	})
	if err != nil {
		return false, err
	}
	/*ok, errInsert := stub.InsertRow("Transaction", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: "10001"}},
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyA"}},
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyB"}},
			&shim.Column{Value: &shim.Column_Int32{Int32: 100}},
			&shim.Column{Value: &shim.Column_Int32{Int32: 1479382905}},
			&shim.Column{Value: &shim.Column_String_{String_: "0"}},
		}, })

	if !ok && errInsert != nil {
		return false, errors.New("Error InsertRow Notification")
	}

	ok, errInsert = stub.InsertRow("Transaction", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: "10002"}},
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyA"}},
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyB"}},
			&shim.Column{Value: &shim.Column_Int32{Int32: 120}},
			&shim.Column{Value: &shim.Column_Int32{Int32: 1479382905}},
			&shim.Column{Value: &shim.Column_String_{String_: "0"}},
		}, })

	if !ok && errInsert != nil {
		return false, errors.New("Error InsertRow Notification")
	}

	ok, errInsert = stub.InsertRow("Transaction", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: "10003"}},
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyB"}},
			&shim.Column{Value: &shim.Column_String_{String_: "CompanyC"}},
			&shim.Column{Value: &shim.Column_Int32{Int32: 30}},
			&shim.Column{Value: &shim.Column_Int32{Int32: 1479382905}},
			&shim.Column{Value: &shim.Column_String_{String_: "0"}},
		}, })

	if !ok && errInsert != nil {
		return false, errors.New("Error InsertRow Notification")
	}*/

	return true, nil
}
// Query callback representing the query of a chaincode
func (t *BankChaincode) Query(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	if function == "get" {
		return t.get(stub, args)
	}
	if function == "listCompany" {
		return t.listCompany(stub, args)
	}
	if function == "searchCompany" {
		return t.searchCompany(stub, args)
	}
	if function == "listTransaction" {
		return t.listTransaction(stub, args)
	}

	if function == "callCompany" {
		chainCodeToCall := "01a37fc6a7e4006184b1e9f06747207ce566033291171d8772fadd72e5374b98b6b3d5095565f3b83ee6a94cbecd8f8fbc5c6ebff50793f39f9b6b4a098ccf93"
		f := "get"
		invokeArgs := util.ToChaincodeArgs(f, "A")
		//response, err := stub.InvokeChaincode(chainCodeToCall, invokeArgs)
		response, err := stub.QueryChaincode(chainCodeToCall, invokeArgs)
		if err != nil {
			errStr := fmt.Sprintf("Failed to invoke chaincode. Got error: %s", err.Error())
			fmt.Printf(errStr)
			return nil, errors.New(errStr)
		}
		fmt.Printf("Invoke chaincode successful. Got response %s", string(response))
		return response, nil
	}

	//TODO add function to get part of JSON object. E.g. 'Company.document.doc1'

	return nil, errors.New("No such method in Query")
}

func (t *BankChaincode) Invoke(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	if function == "update" {
		return t.update(stub, args)
	}
	if function == "remove" {
		return t.remove(stub, args)
	}
	if function == "addCompany" {
		return t.addCompany(stub, args)
	}
	if function == "deleteCompany" {
		return t.deleteCompany(stub, args)
	}
	if function == "addTransaction" {
		return t.addTransaction(stub, args)
	}
	if function == "updateTransaction" {
		return t.updateTransaction(stub, args)
	}

	//TODO add function to update part of JSON object. E.g. 'Company.document.doc1'<-'New document'

	return nil, errors.New("No such method in Invoke")
}

//Get public info
func (t *BankChaincode) get(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	var name string // Entities
	var err error

	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments")
	}

	name = args[0]

	// Get the state from the ledger
	valbytes, err := stub.GetState(name)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + name + "\"}"
		return nil, errors.New(jsonResp)
	}

	if valbytes == nil {
		jsonResp := "{\"Error\":\"Nil value for " + name + "\"}"
		return nil, errors.New(jsonResp)
	}

	cert, err := stub.GetCallerMetadata()
	if err != nil {
		return nil, errors.New("Failed getting metadata.")
	}

	jsonResp := string(valbytes)

	cert2, errCert := stub.ReadCertAttribute("user_type1_0");
	if errCert != nil {
		return nil, errors.New("Failed getting ReadCertAttribute.")
	}

	fmt.Printf("Query Response CERT: %s | cert:%s | cert2:", jsonResp, string(cert), string(cert2))
	return []byte(jsonResp), nil
}
// Update
func (t *BankChaincode) update(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 2 {
		return nil, errors.New("Incorrect number of arguments. Expecting 2")
	}

	name := args[0]
	json := args[1]

	// Update the key from the state in ledger
	err := stub.PutState(name, []byte(json))
	if err != nil {
		return nil, err
	}

	return nil, nil
}

// Deletes an entity from state
func (t *BankChaincode) remove(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting 1")
	}

	name := args[0]

	// Delete the key from the state in ledger
	err := stub.DelState(name)
	if err != nil {
		return nil, errors.New("Failed to delete state")
	}

	return nil, nil
}

func (t *BankChaincode) addCompany(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 4 {
		return nil, errors.New("Incorrect number of arguments. Expecting 4")
	}

	companyName := args[0]
	chaincodeID := args[1]
	country := args[2]
	domain := args[3]

	ok, errInsert := stub.InsertRow("Company", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: companyName}},
			&shim.Column{Value: &shim.Column_String_{String_: chaincodeID}},
			&shim.Column{Value: &shim.Column_String_{String_: country}},
			&shim.Column{Value: &shim.Column_String_{String_: domain}},
		}, })

	if !ok && errInsert != nil {
		return nil, errors.New("Error InsertRow Company")
	}

	return []byte("Ok"), nil
}

//Get list of Companies
func (t *BankChaincode) listCompany(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	rowChannel, err := stub.GetRows("Company",
		[]shim.Column{
		},
	)

	if err != nil {
		return nil, fmt.Errorf("getRowsTableTwo operation failed. %s", err)
	}

	var rows []shim.Row
	for {
		select {
		case row, ok := <-rowChannel:
			if !ok {
				rowChannel = nil
			} else {
				rows = append(rows, row)
			}
		}
		if rowChannel == nil {
			break
		}
	}

	jsonRows, err := json.Marshal(rows)
	if err != nil {
		return nil, fmt.Errorf("getRowsTableTwo operation failed. Error marshaling JSON: %s", err)
	}

	return jsonRows, nil
}

func (t *BankChaincode) searchCompany(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting 1")
	}

	companyName := args[0]

	rowChannel, errorSearch := stub.GetRows("Company",
		[]shim.Column{
			shim.Column{Value: &shim.Column_String_{String_: companyName}},
		}, )

	if errorSearch != nil {
		return nil, fmt.Errorf("Error in search %s", errorSearch)
	}

	var rows []shim.Row
	for {
		select {
		case row, ok := <-rowChannel:
			if !ok {
				rowChannel = nil
			} else {
				rows = append(rows, row)
			}
		}
		if rowChannel == nil {
			break
		}
	}

	jsonRows, err := json.Marshal(rows)
	if err != nil {
		return nil, fmt.Errorf("getRowsTableTwo operation failed. Error marshaling JSON: %s", err)
	}

	return jsonRows, nil
}

func (t *BankChaincode) deleteCompany(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting 1")
	}

	companyName := args[0]

	err := stub.DeleteRow("Company",
		[]shim.Column{
			shim.Column{Value: &shim.Column_String_{String_: companyName}},
		}, )

	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (t *BankChaincode) addTransaction(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 6 {
		return nil, errors.New("Incorrect number of arguments. Expecting 6")
	}

	id := args[0]
	companyNameSender := args[1]
	companyNameReceiver := args[2]

	amount, parseErr := strconv.Atoi(args[3])
	if parseErr != nil {
		return nil, errors.New("Error parsing")
	}

	timestamp, parseErr2 := strconv.ParseInt(args[4], 10, 64)
	if parseErr2 != nil {
		return nil, errors.New("Error parsing")
	}

	suspicion := args[5]
	ok, errInsert := stub.InsertRow("Transaction", shim.Row{
		Columns: []*shim.Column{
			&shim.Column{Value: &shim.Column_String_{String_: id}},
			&shim.Column{Value: &shim.Column_String_{String_: companyNameSender}},
			&shim.Column{Value: &shim.Column_String_{String_: companyNameReceiver}},
			&shim.Column{Value: &shim.Column_Int32{Int32: int32(amount)}},
			&shim.Column{Value: &shim.Column_Int64{Int64: timestamp}},
			&shim.Column{Value: &shim.Column_String_{String_: suspicion}},
		}, })

	if !ok && errInsert != nil {
		return []byte("Error"), errors.New("Error InsertRow Notification")
	}

	return []byte("Ok"), nil
}

func (t *BankChaincode) updateTransaction(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 2 {
		return nil, errors.New("Incorrect number of arguments. Expecting 66")
	}

	id := args[0]
	suspicion := args[1]

	row, errGet := stub.GetRow("Transaction",
		[]shim.Column{
			shim.Column{Value: &shim.Column_String_{String_: id}},
		},)

	if errGet != nil {
		return nil, errors.New("Error InsertRow Transaction")
	}

	row.Columns[5].Value = &shim.Column_String_{String_: suspicion}
	ok, er := stub.ReplaceRow("Transaction", row)

	if !ok && er != nil {
		return nil, er
	}

	return []byte("Ok"), nil
}

func (t *BankChaincode) listTransaction(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	rowChannel, err := stub.GetRows("Transaction",
		[]shim.Column{
		},
	)

	if err != nil {
		return nil, fmt.Errorf("getRowsTableTwo operation failed. %s", err)
	}

	var i int64 = 0
	offset, parseErr := strconv.ParseInt(args[0], 10, 64)
	if parseErr != nil {
		return nil, errors.New("Error parsing")
	}

	size, parseErr2 := strconv.ParseInt(args[1], 10, 64)
	if parseErr2 != nil {
		return nil, errors.New("Error parsing 2")
	}


	var rows []shim.Row
	for {
		select {
		case row, ok := <-rowChannel:
			if !ok {
				rowChannel = nil
			} else {

				if i >= offset {
					rows = append(rows, row)
				}

				i = i + 1

				if i == offset + size {
					jsonRows, err := json.Marshal(rows)
					if err != nil {
						return nil, fmt.Errorf("getRowsTableTwo operation failed. Error marshaling JSON: %s", err)
					}
					return jsonRows, nil
				}
			}
		}
		if rowChannel == nil {
			break
		}
	}

	jsonRows, err := json.Marshal(rows)
	if err != nil {
		return nil, fmt.Errorf("getRowsTableTwo operation failed. Error marshaling JSON: %s", err)
	}

	return jsonRows, nil
}

func (t *BankChaincode) isCaller(stub shim.ChaincodeStubInterface, certificate []byte) (bool, error) {
	sigma, err := stub.GetCallerMetadata()
	if err != nil {
		return false, errors.New("Failed getting metadata")
	}
	payload, err := stub.GetPayload()
	if err != nil {
		return false, errors.New("Failed getting payload")
	}
	binding, err := stub.GetBinding()
	if err != nil {
		return false, errors.New("Failed getting binding")
	}

	ok, err := stub.VerifySignature(
		certificate,
		sigma,
		append(payload, binding...),
	)
	if err != nil {
		return ok, err
	}

	return ok, err
}
func main() {
	err := shim.Start(new(BankChaincode))
	if err != nil {
		fmt.Printf("Error starting chaincode: %s", err)
	}
}
