package com.client;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import javax.xml.bind.JAXB;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by root on 17.11.2016.
 */
public class Client {

    private void post() {
        try {
            URL url = new URL("https://f146d0f22c0546489107a179e4783c5d-vp0.us.blockchain.ibm.com:5001/chaincode");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/xml");

            String post = "{\n" +
                    "  \"jsonrpc\": \"2.0\",\n" +
                    "  \"method\": \"query\",\n" +
                    "  \"params\": {\n" +
                    "    \"type\": 1,\n" +
                    "    \"chaincodeID\": {\n" +
                    "      \"name\": \"60d1bc705a31fcdf8203bd63f3214e33a26abcbbb6e3d2c01a9c511d1e16fa15afd5cce3120fa6b85d6d9c2252ba1d4bd29c8bc4044852916196f24681ecf6e9\"\n" +
                    "    },\n" +
                    "    \"ctorMsg\": {\n" +
                    "      \"function\": \"listCompany\",\n" +
                    "      \"args\": [\n" +
                    "\n" +
                    "      ]\n" +
                    "    },\n" +
                    "    \"secureContext\": \"user_type1_0\"\n" +
                    "  },\n" +
                    "  \"id\": 1234\n" +
                    "}";

            OutputStream os = connection.getOutputStream();
            os.write(post.getBytes());
//            jaxbContext.createMarshaller().marshal(customer, os);
            os.flush();

            int code = connection.getResponseCode();

            StringBuilder builder = new StringBuilder();
            int read = 0;
            byte[] buff = new byte[1024];
            InputStream inputStream = connection.getInputStream();
            while ((read = inputStream.read(buff)) != -1) {
                builder.append(new String(buff, 0, read));
            }

            Gson gson = new Gson();
            HashMap hashMap = gson.fromJson(builder.toString(), HashMap.class);
            String message  = (String) ((LinkedTreeMap) hashMap.get("result")).get("message");
            HashMap array = gson.fromJson("{array:" + message + "}", HashMap.class);
            ArrayList<LinkedTreeMap> values = (ArrayList<LinkedTreeMap>) array.get("array");
            for (LinkedTreeMap value : values) {
                ArrayList<LinkedTreeMap> columns =((ArrayList<LinkedTreeMap>) value.get("columns"));
                for (LinkedTreeMap column : columns) {
                    System.out.print(((LinkedTreeMap) column.get("Value")).values().iterator().next());
                    System.out.print("\t");
                }
                System.out.println();
            }

            connection.disconnect();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<List<String>> post(String postBody) {
        List<List<String>> result = new ArrayList<>();
        try {
            URL url = new URL("https://f146d0f22c0546489107a179e4783c5d-vp0.us.blockchain.ibm.com:5001/chaincode");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/xml");

            OutputStream os = connection.getOutputStream();
            os.write(postBody.getBytes());
            os.flush();

            int code = connection.getResponseCode();

            StringBuilder builder = new StringBuilder();
            int read = 0;
            byte[] buff = new byte[1024];
            InputStream inputStream = connection.getInputStream();
            while ((read = inputStream.read(buff)) != -1) {
                builder.append(new String(buff, 0, read));
            }



            Gson gson = new Gson();
            HashMap hashMap = gson.fromJson(builder.toString(), HashMap.class);
            String message  = (String) ((LinkedTreeMap) hashMap.get("result")).get("message");
            HashMap array = gson.fromJson("{array:" + message + "}", HashMap.class);
            ArrayList<LinkedTreeMap> values = (ArrayList<LinkedTreeMap>) array.get("array");
            for (LinkedTreeMap value : values) {
                List<String> row = new ArrayList<>();

                ArrayList<LinkedTreeMap> columns =((ArrayList<LinkedTreeMap>) value.get("columns"));
                for (LinkedTreeMap column : columns) {
//                    System.out.print(((LinkedTreeMap) column.get("Value")).values().iterator().next());
//                    System.out.print("\t");
                    row.add(((LinkedTreeMap) column.get("Value")).values().iterator().next().toString());
                }
//                System.out.println();
                result.add(row);
            }

            connection.disconnect();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    public static void main(String[] args) throws IOException {
//        new Client().post();
        new Client().post(Util.readFile("backend\\blockchain\\hyperledger\\resources\\script\\listCompany.json", Charset.defaultCharset()));
    }
}
